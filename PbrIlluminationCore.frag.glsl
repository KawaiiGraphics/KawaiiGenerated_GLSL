#version 450 core

precision mediump int; precision highp float;

struct MaterialInfo_PbrIlluminationCore
{
    vec3 albedo;
    float metallic;
    float roughness;
    float ao;
};

MaterialInfo_PbrIlluminationCore pbr_material = MaterialInfo_PbrIlluminationCore(vec3(0.1), 0.5, 0.5, 1.0);

const float PI = 3.14159265359;

vec3 fresnelSchlick(float cosTheta, vec3 F0)
{
    return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
}

float distributionGGX(vec3 normal, vec3 H, float roughness)
{
    float a      = roughness*roughness;
    float a2     = a*a;
    float NdotH  = max(dot(normal, H), 0.0);
    float NdotH2 = NdotH * NdotH;

    float num   = a2;
    float denom = (NdotH2 * (a2 - 1.0) + 1.0);
    denom = PI * denom * denom;

    return num / denom;
}

float geometrySchlickGGX(float n_dot_v, float roughness)
{
    float r = (roughness + 1.0);
    float k = (r*r) / 8.0;

    float num   = n_dot_v;
    float denom = n_dot_v * (1.0 - k) + k;

    return num / denom;
}

float geometrySmith(vec3 N, vec3 V, vec3 L, float roughness)
{
    float NdotV = max(dot(N, V), 0.0);
    float NdotL = max(dot(N, L), 0.0);
    float ggx2  = geometrySchlickGGX(NdotV, roughness);
    float ggx1  = geometrySchlickGGX(NdotL, roughness);

    return ggx1 * ggx2;
}

vec3 getLo_PbrIlluminationCore(in vec3 n, in vec3 light_dir, in vec3 to_camera, in vec3 light_color)
{
    vec3 toCam = to_camera;
    vec3 lightDir = light_dir;
    if(length(to_camera - light_dir) <= 0.001)
    {
        toCam = normalize(to_camera + vec3(0.005));
        lightDir = normalize(light_dir - vec3(0.005));
    }

    vec3 F0 = vec3(0.04);
    F0 = mix(F0, pbr_material.albedo, pbr_material.metallic);
    vec3 H = normalize(toCam + light_dir);

    // Cook-Torrance BRDF
    float NDF = distributionGGX(n, H, pbr_material.roughness);
    float G   = geometrySmith(n, toCam, light_dir, pbr_material.roughness);
    vec3 F    = fresnelSchlick(max(dot(H, toCam), 0.0), F0);

    vec3 kS = F;
    vec3 kD = vec3(1.0) - kS;
    kD *= 1.0 - pbr_material.metallic;

    vec3 numerator    = NDF * G * F;
    float denominator = 4.0 * max(dot(n, toCam), 0.0) * max(dot(n, light_dir), 0.0) + 0.001;
    vec3 specular     = numerator / denominator;

    float NdotL = max(dot(n, light_dir), 0.0);
    return (kD * pbr_material.albedo / PI + specular) * light_color * NdotL;
}

vec3 getAmbient_PbrIlluminationCore(in vec3 light_ambient_sum)
{
    return light_ambient_sum * pbr_material.albedo * pbr_material.ao;
}

